/* Homework: LTS & FSP
*
*
*
* Name: Sergi
* Surname: Granell Escalfet
*
* Name: Guillem
* Surname: Lopez Paradis
*
*/
/* museum */

const N = 5
EAST = (arrive -> EAST).
WEST = (leave -> WEST).
DIRECTOR = (open -> close -> DIRECTOR).
CONTROL = CLOSED[0],
CLOSED[i:0..N] = (when (i==0) open -> OPENED[0]
                 |when (i>0) leave -> CLOSED[i-1]
),
OPENED[i:0..N] = (close -> CLOSED[i]
                 |when(i<N) arrive -> OPENED[i+1]
                 |when(i>0) leave -> OPENED[i-1]
).
||MUSEUM = (EAST || WEST || DIRECTOR || CONTROL).

/* Time-Out Client server */

/* Deadlock problem:
There is a deadlock due to timeout not being a shared action.
The deadlock can be reached by two different paths:
  1) call -> service -> timeout: now the server can't go back to the
     initial state because reply is a shared action and client can't reach it.
  2) call -> timeout -> service: same as before, the server expects to do
     reply but the client can't reach a state to be able to do such.

The solution is straighforward and is to make timeout a shared action,
so if the server gets a timeout then it has to return to the initial state.
*/

/* write your solution */

CLIENT = (call -> WAIT),
WAIT = (answer -> continue -> CLIENT
       |timeout -> CLIENT).

SERVER  = (request -> REQUEST),
REQUEST = (timeout -> SERVER
          |service -> SERVICE),
SERVICE = (reply -> SERVER
          |timeout -> SERVER).

||CLIENT_SERVER = (CLIENT || SERVER) /{call/request, reply/answer}.

/* write your solution */
/* end homework */
