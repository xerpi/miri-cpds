
public class Savage extends Thread{
    BadPot pot;
    public Savage(BadPot pot) {
        this.pot = pot;
    }
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " is hungry ... ");
            try {
                Thread.sleep(200);
                pot.getserving();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}