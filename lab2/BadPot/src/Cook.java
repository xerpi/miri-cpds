
public class Cook extends Thread{
    BadPot pot;
    public Cook(BadPot pot) {
        this.pot=pot;
    }
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " fills the pot");
            try {
                Thread.sleep(200);
                pot.fillpot();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}