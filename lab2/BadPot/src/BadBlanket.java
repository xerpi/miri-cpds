
public class BadBlanket {
    public static void main(String args[]) {
        BadPot pot = new BadPot(5);
        Thread a = new Savage(pot);
        a.setName("alice");
        Thread b = new Savage(pot);
        b.setName("bob");
        Thread c = new Cook(pot);
        c.setName("Charlie");
        a.start();
        b.start();
        c.start();
    }
}
