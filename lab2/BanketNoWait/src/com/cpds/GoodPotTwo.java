package com.cpds;

import java.util.concurrent.atomic.AtomicInteger;

public class GoodPotTwo {
	private AtomicInteger servings;
	private int capacity;

	public GoodPotTwo(int capacity) {
		this.capacity = capacity;
		servings = new AtomicInteger(0);
	}

	public void getserving() throws InterruptedException {
		if (servings.get() == 0) {
			System.out.println(Thread.currentThread().getName() + " go walk");
		} else {
			Thread.sleep(200);
			int curServings = servings.get();
			if (curServings == 0 || !servings.compareAndSet(curServings, curServings - 1)) {
				System.out.println(Thread.currentThread().getName() + " go walk");
			} else {
				System.out.println(Thread.currentThread().getName() + " is served");
				print_servings();
			}
		}
	}

	public void fillpot() throws InterruptedException {
		if (servings.get() > 0) {
			System.out.println(Thread.currentThread().getName() + " go walk");
		} else {
			Thread.sleep(200);
			if (servings.compareAndSet(0, capacity)) {
				print_servings();
			} else {
				System.out.println(Thread.currentThread().getName() + " go walk");
			}
		}
	}

	public void print_servings() {
		System.out.println("servings in the pot: " + servings.get());
	}
}