package com.cpds;

public class Cook extends Thread {
    GoodPotTwo pot;
    public Cook(GoodPotTwo pot) {
        this.pot=pot;
    }
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " fills the pot");
            try {
                Thread.sleep(200);
                pot.fillpot();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
