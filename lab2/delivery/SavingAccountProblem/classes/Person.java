package com.cpds;

import java.util.Random;

public class Person extends Thread {
	private static final Random random = new Random();
	private Account account;

	public Person(Account account) {
		this.account = account;
	}

	public void run() {
		while (true) {
			if (random.nextBoolean()) {
				int amount = random.nextInt(5) + 1;

				System.out.println(Thread.currentThread().getName() +
					" will deposit " + amount);

				try {
					Thread.sleep(200);
					account.deposit(amount);
				} catch (InterruptedException e) {}
			} else {
				int amount = random.nextInt(100) + 1;

				System.out.println(Thread.currentThread().getName() +
					" will withdraw " + amount);

				try {
					Thread.sleep(200);
					account.withdraw(amount);
				} catch (InterruptedException e) {}
			}
		}
	}
}
