package com.cpds;

public class Account {
	private int balance;

	public Account() {
		balance = 0;
	}

	public synchronized void deposit(int amount) {
		balance += amount;

		printBalance();

		notifyAll();
	}

	public synchronized void withdraw(int amount) throws InterruptedException {
		while (amount > balance)
			wait();

		balance -= amount;
		System.out.println(Thread.currentThread().getName() +
			" has withdrawn " + amount);
		printBalance();

		notifyAll();
	}

	private synchronized void printBalance() {
		System.out.println("Current balance: " + balance);
	}
}
