package com.cpds;

import java.util.Random;

public class Company extends Thread {
	private static final Random random = new Random();
	private Account account;

	public Company(Account account) {
		this.account = account;
	}

	public void run() {
		while (true) {
			int amount = random.nextInt(10) + 1;
			System.out.println(Thread.currentThread().getName() +
				" will deposit " + amount);

			try {
				Thread.sleep(200);
				account.deposit(amount);
			} catch (InterruptedException e) {}
		}
	}
}
