#!/bin/bash
# @ job_name		= heatCUDA-ex4
# @ initialdir		= .
# @ output		= heatCUDA-ex4.exec.%j.out
# @ error		= heatCUDA-ex4.exec.%j.err
# @ total_tasks		= 1
# @ gpus_per_node	= 1
# @ wall_clock_limit	= 00:02:0

#LD_LIBRARY_PATH="/opt/cuda/8.0/lib64"

executable=./heatCUDA-ex4
# Define threads per block
txb=16

${executable} test.dat -t $txb
