#include <math.h>
#include <float.h>
#include <cuda.h>

__global__ void gpu_Heat(float *h, float *g, int N, float *block_res)
{
	// h -> u
	// g -> utmp

	extern __shared__ float res_tmp[];

	int i = blockIdx.y * blockDim.y + threadIdx.y;
	int j = blockIdx.x * blockDim.x + threadIdx.x;
	int res_idx = threadIdx.x + blockDim.x * threadIdx.y;

	// Need to check j-1>0 and j+1<N... etc
	if (i > 0 && i < (N - 1) && j > 0 && j < (N - 1)) {
		g[i*N + j] = 0.25 * (h[i * N + (j - 1)] +  // left
				     h[i * N + (j + 1)] +  // right
				     h[(i - 1) * N + j] +  // top
				     h[(i + 1) * N + j]);  // bottom
		// Residual calc
		float diff = g[i*N + j] - h[i*N + j];
		res_tmp[res_idx] = diff * diff;
	}

	// Block-level reduction
	for (unsigned int s = (blockDim.x * blockDim.y) / 2; s > 0; s >>= 1) {
		if (res_idx < s)
			res_tmp[res_idx] += res_tmp[res_idx + s];
		__syncthreads();
	}

	// Copy back the reduced value to global memory
	if (res_idx == 0)
		block_res[blockIdx.x + gridDim.x * blockIdx.y] = res_tmp[0];
}
