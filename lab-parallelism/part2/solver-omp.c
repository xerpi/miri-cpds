#include "heat.h"

#define NB 8

#define min(a, b) (((a) < (b)) ? (a) : (b))

/*
 * Blocked Jacobi solver: one iteration step
 */
double relax_jacobi(double *u, double *utmp, unsigned sizex, unsigned sizey)
{
	double diff, sum = 0.0;
	int nbx, bx, nby, by;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	/*
	 * 12 doesn't divide 8*8=64 so in theory it should be better
	 * to use a dynamic schedule but they have greater overhead.
	 * In this algorithm copying utmp to tmp after each iteration
	 * has a lot of overhead that kills linear speedups.
	 *
	 * u is read-only, and utmp is write-only => no dependencies
	 */
	#pragma omp parallel for schedule(dynamic) collapse(2) private(diff) reduction(+:sum)
	for (int ii = 0; ii < nbx; ii++) {
		for (int jj = 0; jj < nby; jj++) {
			for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
				for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
					utmp[i * sizey + j] = 0.25 * (u[i * sizey + (j - 1)] +  // left
								      u[i * sizey + (j + 1)] +  // right
								      u[(i - 1) * sizey + j] +  // top
								      u[(i + 1) * sizey + j]);  // bottom
					diff = utmp[i * sizey + j] - u[i * sizey + j];
					sum += diff * diff;
				}
			}
		}
	}

	return sum;
}

/*
 * Blocked Red-Black solver: one iteration step
 */
double relax_redblack(double *u, unsigned sizex, unsigned sizey)
{
	double unew, diff, sum = 0.0;
	int nbx, bx, nby, by;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	/*
	 * Even though 'u' is read and written to, in this case first only
	 * the Red blocks are executed and then the Black blocks
	 * => no dependencies between blocks but within Red and Black runs
	 * R B R B R B ...
	 * B R B R B R ...
	 * R B R B R B ...
	 * ...
	 * Since NB is 8 we can only take advantage of 8 threads => collapse
	 */

	// Computing "Red" blocks
	#pragma omp parallel for collapse(2) private(diff, unew) reduction(+:sum)
	for (int ii = 0; ii < nbx; ii++) {
		for (int jjj = 0; jjj < nby; jjj+=2) {
			int jj = jjj + ii % 2;
			for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
				for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
					unew = 0.25 * (u[i * sizey + (j - 1)] + // left
						       u[i * sizey + (j + 1)] + // right
						       u[(i - 1) * sizey + j] + // top
						       u[(i + 1) * sizey + j]); // bottom
					diff = unew - u[i * sizey + j];
					sum += diff * diff;
					u[i * sizey + j] = unew;
				}
			}
		}
	}

	// Computing "Black" blocks
	#pragma omp parallel for collapse(2) private(diff, unew) reduction(+:sum)
	for (int ii = 0; ii < nbx; ii++) {
		for (int jjj = 0; jjj < nby; jjj+=2) {
			int jj = jjj + (ii + 1) % 2;
			for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
				for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
					unew = 0.25 * (u[i * sizey + (j - 1)] + // left
						       u[i * sizey + (j + 1)] + // right
						       u[(i - 1) * sizey + j] + // top
						       u[(i + 1) * sizey + j]); // bottom
					diff = unew - u[i * sizey + j];
					sum += diff * diff;
					u[i * sizey + j] = unew;
				}
			}
		}
	}

	return sum;
}

/*
 * Blocked Gauss-Seidel solver: one iteration step
 */
double relax_gauss(double *u, unsigned sizex, unsigned sizey)
{
	double unew, diff, sum = 0.0;
	int nbx, bx, nby, by;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	/*
	 * 'u' is read and written to, so we have dependencies
	 * we execute with a wavefront pattern:
	 * ####
	 *  ####
	 *   ####
	 * Since we are dividing into 8x8 blocks, at most there will be
	 * 8 tasks that will be able to execute at the same time (diagonal).
	 * The best would be to change nbx and nby to omp_get_num_threads();
	 */

	#pragma omp parallel
	{
		#pragma omp single
		for (int ii = 0; ii < nbx; ii++) {
			for (int jj = 0; jj < nby; jj++) {
				int up, down, left, right;

				up = (ii > 0) ? ii - 1 : 0;
				down = (ii < nbx) ? ii + 1 : nbx;
				left = (jj > 0) ? jj - 1 : 0;
				right = (jj < nby) ? jj : nby;

				#define IDX(row, col) \
					((row) * nby * sizey + (col) * bx)

				#pragma omp task private(diff, unew) \
					depend(in: u[IDX(up, jj)], u[IDX(down, jj)], u[IDX(ii, left)], u[IDX(ii, right)]) \
					depend(out: u[IDX(ii, jj)])
				{
					double lsum = 0.0;
					for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
						for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
							unew = 0.25 * (u[i * sizey + (j - 1)] + // left
								       u[i * sizey + (j + 1)] + // right
								       u[(i - 1) * sizey + j] + // top
								       u[(i + 1) * sizey + j]); // bottom
							diff = unew - u[i * sizey + j];
							lsum += diff * diff;
							u[i * sizey + j] = unew;
						}
					}
					#pragma omp atomic
					sum += lsum;
				}
			}
		}
	}

	return sum;
}
