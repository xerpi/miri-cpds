#include "heat.h"

#define NB 8

#define min(a, b) (((a) < (b)) ? (a) : (b))

/*
 * Blocked Jacobi solver: one iteration step
 */
double relax_jacobi(double *u, double *utmp, unsigned sizex, unsigned sizey)
{
	double diff, sum = 0.0;
	int nbx, bx, nby, by;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	/*
	 * 12 doesn't divide 8*8=64 so in theory it should be better
	 * to use a dynamic schedule but they have greater overhead.
	 * In this algorithm copying utmp to tmp after each iteration
	 * has a lot of overhead that kills linear speedups.
	 */
	#pragma omp parallel for collapse(2) private(diff) reduction(+:sum)
	for (int ii = 0; ii < nbx; ii++) {
		for (int jj = 0; jj < nby; jj++) {
			for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
				for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
					utmp[i * sizey + j] = 0.25 * (u[i * sizey + (j - 1)] +  // left
								      u[i * sizey + (j + 1)] +  // right
								      u[(i - 1) * sizey + j] +  // top
								      u[(i + 1) * sizey + j]);  // bottom
					diff = utmp[i * sizey + j] - u[i * sizey + j];
					sum += diff * diff;
				}
			}
		}
	}

	return sum;
}

/*
 * Blocked Red-Black solver: one iteration step
 */
double relax_redblack(double *u, unsigned sizex, unsigned sizey)
{
	double unew, diff, sum = 0.0;
	int nbx, bx, nby, by;
	int lsw;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	#pragma omp parallel
	{
		// Computing "Red" blocks
		#pragma omp single
		for (int ii = 0; ii < nbx; ii++) {
			lsw = ii % 2;
			for (int jj = lsw; jj < nby; jj = jj + 2) {
				#pragma omp task private(diff) \
					depend(in: u[0]) \
					depend(out: u[0])
				{
					double lsum = 0.0;
					for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
						for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
							unew = 0.25 * (u[i * sizey + (j - 1)] + // left
								       u[i * sizey + (j + 1)] + // right
								       u[(i - 1) * sizey + j] + // top
								       u[(i + 1) * sizey + j]); // bottom
							diff = unew - u[i * sizey + j];
							lsum += diff * diff;
							u[i * sizey + j] = unew;
						}
					}
					#pragma omp atomic
					sum += lsum;
				}
			}
		}

		// Computing "Black" blocks
		#pragma omp single
		for (int ii = 0; ii < nbx; ii++) {
			lsw = (ii + 1) % 2;
			for (int jj = lsw; jj < nby; jj = jj + 2) {
				#pragma omp task private(diff)
				{
					double lsum = 0.0;
					for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
						for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
							unew = 0.25 * (u[i * sizey + (j - 1)] + // left
								       u[i * sizey + (j + 1)] + // right
								       u[(i - 1) * sizey + j] + // top
								       u[(i + 1) * sizey + j]); // bottom
							diff = unew - u[i * sizey + j];
							lsum += diff * diff;
							u[i * sizey + j] = unew;
						}
					}
					#pragma omp atomic
					sum += lsum;
				}
			}
		}
	}

	return sum;
}

/*
 * Blocked Gauss-Seidel solver: one iteration step
 */
double relax_gauss(double *u, unsigned sizex, unsigned sizey)
{
	double unew, diff, sum = 0.0;
	int nbx, bx, nby, by;

	nbx = NB;
	bx = sizex / nbx;
	nby = NB;
	by = sizey / nby;

	for (int ii = 0; ii < nbx; ii++) {
		for (int jj = 0; jj < nby; jj++) {
			for (int i = 1 + ii * bx; i <= min((ii + 1) * bx, sizex - 2); i++) {
				for (int j = 1 + jj * by; j <= min((jj + 1) * by, sizey - 2); j++) {
					unew = 0.25 * (u[i * sizey + (j - 1)] + // left
						       u[i * sizey + (j + 1)] + // right
						       u[(i - 1) * sizey + j] + // top
						       u[(i + 1) * sizey + j]); // bottom
					diff = unew - u[i * sizey + j];
					sum += diff * diff;
					u[i * sizey + j] = unew;
				}
			}
		}
	}

	return sum;
}
