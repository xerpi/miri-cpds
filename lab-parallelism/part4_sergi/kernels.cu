#include <math.h>
#include <float.h>
#include <cuda.h>

__global__ void gpu_Heat(float *h, float *g, int N)
{
	int i = blockDim.y * blockIdx.y + threadIdx.y;
	int j = blockDim.x * blockIdx.x + threadIdx.x;

	/* Check bounds */
	if (i > 0 && j > 0 && i < N - 1 && j < N - 1) {
		g[i * N + j] = 0.25 * (h[i * N + (j - 1)] +  // left
				       h[i * N + (j + 1)] +  // right
				       h[(i - 1) * N + j] +  // top
				       h[(i + 1) * N + j]);  // bottom
	}
}
