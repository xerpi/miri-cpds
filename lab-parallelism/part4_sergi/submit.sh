#!/bin/bash
# @ job_name		= heatCUDA.exec
# @ initialdir		= .
# @ output		= heatCUDA.exec.%j.out
# @ error		= heatCUDA.exec.%j.err
# @ total_tasks		= 1
# @ gpus_per_node	= 1
# @ wall_clock_limit	= 00:02:00

executable=./heatCUDA
# Define threads per block
txb=16

 ${executable} test.dat -t $txb
