#!/bin/bash
# @ job_name		= heat
# @ initialdir		= .
# @ output		= heat.%j.out
# @ error		= heat.%j.err
# @ total_tasks		= 1
# @ cpus_per_task	= 1
# @ tasks_per_node	= 1
# @ wall_clock_limit	= 00:02:00

prog=heat

srun ./$prog test.dat
