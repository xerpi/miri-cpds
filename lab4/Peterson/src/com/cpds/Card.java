package com.cpds;

public class Card {
	private int value;

	public Card() {
		value = 1;
	}

	public synchronized void giveTurnOther(String s) {
		/* Give the turn to the other neighbor */
		if (s.equals("alice"))
			value = 2;
		else
			value = 1;
	}

	public synchronized boolean hasTurn(String s) {
		/* Check if we have the turn */
		if (s.equals("alice"))
			return value == 1;
		else
			return value == 2;
	}
}
