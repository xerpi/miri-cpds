package com.cpds;

public class Main {
	public static void main(String[] args) {
		Flags flags = new Flags();
		Card card = new Card();

		/*
		Thread a = new Neighbor(flags, card);
		Thread b = new Neighbor(flags, card);
		*/

		/* Even with greedy neighbors we don't have
		 * any kind of starvation (thanks to the Peterson's
		 * algorithm).
		 */
		Thread a = new NeighborGreedy(flags, card);
		Thread b = new NeighborGreedy(flags, card);

		a.setName("alice");
		b.setName("bob");

		a.start();
		b.start();
	}
}
