package com.cpds;

public class Neighbor extends Thread {
	private Flags flags;
	private Card card;

	public Neighbor(Flags flags, Card card) {
		this.flags = flags;
		this.card = card;
	}

	public void run() {
		while (true) {
			try {
				String name = Thread.currentThread().getName();
				System.out.println("try again, my name is: " + name);

				Thread.sleep((int)(200*Math.random()));

				flags.set_true(name);

				card.giveTurnOther(name);

				/* We can only continue if the other neighbor has NOT
				 * raised their flag or they have raised the flag but
				 * we have the turn.
				 */
				while (flags.query_flag(name) && !card.hasTurn(name)) {
					/* Busy-spin... */
				};

				System.out.println(name + " enter");
				Thread.sleep(400);
				System.out.println(name + " exits");

				Thread.sleep((int)(200*Math.random()));

				flags.set_false(name);

			} catch (InterruptedException e) {};
		}
	}
}