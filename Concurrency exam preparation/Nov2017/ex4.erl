-module(ex4).
-compile(export_all).

dot_product([H1|T1], [H2|T2]) -> H1 * H2 + dot_product(T1, T2);
dot_product([], []) -> 0.

sep(L, 0) -> {[], L};
sep([H|T], N) ->
	{L, R} = sep(T, N - 1),
	{[H | L], R}.

% L5=[1,2,3,4,5,6,7,8,9,10].
% L6=[10,9,8,7,6,5,4,3,2,1].
% L1 and L2 have the same size
par_dot_product(L1, L2) ->
	Pid = spawn(ex4, par_dot_product, [self(), L1, L2]),
	receive
		{Pid, Res} -> Res
	end.

par_dot_product(Pid, L1, L2) when length(L1) < 1000 ->
	Pid ! {self(), dot_product(L1, L2)};
par_dot_product(Pid, L1, L2) ->
	{L1l, L1r} = sep(L1, length(L1) div 2),
	{L2l, L2r} = sep(L2, length(L2) div 2),
	Pid1 = spawn(ex4, par_dot_product, [self(), L1l, L2l]),
	Pid2 = spawn(ex4, par_dot_product, [self(), L1r, L2r]),
	receive
		{Pid1, Res1} -> Res1
	end,
	receive
		{Pid2, Res2} -> Res1
	end,
	Pid ! {self(), Res1 + Res2}.
