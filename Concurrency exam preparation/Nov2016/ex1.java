/*
 * http://syllabus.cs.manchester.ac.uk/ugt/2017/COMP31212/misc/exercises3-withanswers.pdf
 */

public synchronized arrive() { /* wrong */
	if (!opened)
		return;

	while (i == N) /* opened MUST be checked here */
		wait();
	i++;
	notifyAll();
}
