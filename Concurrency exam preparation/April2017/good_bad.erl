-module(good_bad).
-export([good_worker/0, bad_worker/0]).

bad_worker() ->
	receive
	{add, V1, V2} ->
		io:format("Result is ~p ~n", [V1 + V2]);
	{sub, V1, V2} ->
		io:format("Result is ~p ~n", [V1 - V2]);
	{mul, V1, V2} ->
		io:format("Result is ~p ~n", [V1 * V2]);
	{square, V1} ->
		io:format("Result is ~p ~n", [V1 * V1]);
	_ ->
		io:format("I can’t work on this! ~n")
	end.

good_worker() ->
	receive
	{add, V1, V2} ->
		io:format("Result is ~p ~n", [V1 + V2]);
	{sub, V1, V2} ->
		io:format("Result is ~p ~n", [V1 - V2]);
	{mul, V1, V2} ->
		io:format("Result is ~p ~n", [V1 * V2]);
	{square, V1} ->
		io:format("Result is ~p ~n", [V1 * V1]);
	_ ->
		io:format("I can’t work on this! ~n")
	end,
	good_worker().
