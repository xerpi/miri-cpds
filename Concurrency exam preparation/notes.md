---
fontsize: 8pt
geometry: margin=3cm
---

# LTS

## Process naming
`a:P` prefixes each action label in the alphabet of P with a.

```
SWITCH = (on ->off->SWITCH).
||TWO_SWITCH = (alice:SWITCH || bob:SWITCH).
```

## Process labeling by a set of prefix labels

Given P,

* `{a1,...,aX}::P` replaces every action label `n` with the labels `a1.n`,..., `aX.n`.
* Further, every transition `n->X` in the definition of P is replaced
  with the transitions `{a1.n,...,aX.n}->X`.

```
RESOURCE = (acquire->release->RESOURCE).
USER = (acquire->use->release->USER).
||RESOURCE_SHARE = (a:USER || b:USER || {a,b}::RESOURCE).
```

## Action relabeling

`/{newlabel 1/oldlabel 1,...,newlabel n/oldlabel n}`

```
CLIENT = (call->wait->continue->CLIENT).
SERVER = (request->service->reply->SERVER).
||CLIENT SERVER = (CLIENT || SERVER)/{call/request, reply/wait}.
```

## Safety properties

```
property SAFE = (
	a.call -> a.answer -> SAFE |
	b.call -> b.answer -> SAFE
).
```

## Progress properties

A set `P = {a1,a2,...,aN}` defines a progress property `P` which asserts
that in an infinite execution of a target system,
at least one of the actions `a1,a2,...,aN` will be executed infinitely often.

```
COIN = (toss->heads->COIN | toss->tails->COIN).
progress HEADS = {heads}
progress TAILS = {tails}
```

## Action priority

**High priority** (<<): `||C = (P||Q) << {a1,...,aN}`
In any choice in this system which has one or more of the
actions `a1,...,aN` labeling a transition, the transitions labeled
with lower priority actions are discarded.

**Low priority** (>>): `||C = (P||Q) >> {a1,...,aN}`
In any choice in this system which has one or more
transitions not labeled by `a1,...,aN`, the transitions labeled
by `a1,...,aN` are discarded.

# Erlang

```erlang
sum([H|T]) -> H + sum(T);
sum([]) -> 0.
```

```erlang
sep(L, 0) -> {[], L};
sep([H|T], N) ->
	{L1, L2} = sep(T, N-1),
	{[H|L1], L2}.
```

```erlang
merge(L1, []) -> L1;
merge([], L2) -> L2;
merge([H1 | T1], [H2 | T2]) ->
	if H1 < H2 ->
		[H1 | merge(T1, [H2 | T2])];
	true ->
		[H2 | merge([H1 | T1], T2)]
	end.

rcvp(Pid) ->
	receive
		{Pid, L} -> L
	end.
pms(L) ->
	Pid = spawn(msort, p_ms, [self(), L]),
	rcvp(Pid).
p_ms(Pid, L) when length(L) < 100 ->
	Pid ! {self(), ms(L)};
p_ms(Pid, L) ->
	{Lleft, Lright} = sep(L, length(L) div 2),
	Pid1 = pms(Lleft),
	Pid2 = pms(Lright),
	L1 = rcvp(Pid1),
	L2 = rcvp(Pid2),
	Pid ! {self(), merge(L1, L2)}.
```

$S = \{ 2 \cdot x | x \in \{0, ..., 100\}, x^2 > 3, x^2 < 50\}$
```erlang
 S = [2*X || X <- lists:seq(0,100), X*X > 3, X*X<50].
```

