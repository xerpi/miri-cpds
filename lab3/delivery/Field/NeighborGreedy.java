package com.cpds;

public class NeighborGreedy extends Thread {
	private Flags flags;

	public NeighborGreedy(Flags flags) {
		this.flags = flags;
	}

	public void run() {
		while (true) {
			try {
				String name = Thread.currentThread().getName();
				System.out.println("try again, my name is: " + name);

				flags.set_true(name);

				Thread.sleep((int)(200*Math.random()));

				/* We can only enter if the other neighbor has NOT
				 * raised their flag!
				 */
				if (!flags.query_flag(name)) {
					System.out.println(name + " enter");
					Thread.sleep(400);
					System.out.println(name + " exits");
				}

				Thread.sleep((int)(200*Math.random()));

				flags.set_false(name);
			} catch (InterruptedException e) {};
		}
	}
}
