/*
 * Name: Sergi
 * Surname: Granell Escalfet
 *
 * Name: Guillem
 * Surname: Lopez Paradis
 */

const False = 0
const True = 1
range Bool = False..True
set BoolActions = {setTrue, setFalse, [False], [True]}

BOOLVAR = VAL[False],
VAL[v:Bool] = (
	setTrue -> VAL[True] |
	setFalse -> VAL[False] |
	[v] -> VAL[v]
).

||FLAGS = (flag1:BOOLVAR || flag2:BOOLVAR).

NEIGHBOR1 = (flag1.setTrue -> TEST),
TEST = (
	flag2[raised:Bool] ->
		if (raised) then
			(flag1.setFalse -> NEIGHBOR1)
		else
			(enter -> exit -> flag1.setFalse -> NEIGHBOR1)
) + {{flag1,flag2}.BoolActions}.

NEIGHBOR2 = (flag2.setTrue -> TEST),
TEST = (
	flag1[raised:Bool] ->
		if (raised) then
			(flag2.setFalse -> NEIGHBOR2)
		else
			(enter -> exit -> flag2.setFalse -> NEIGHBOR2)
) + {{flag1,flag2}.BoolActions}.

property MUTEX = (
	n1.enter -> n1.exit -> MUTEX |
	n2.enter -> n2.exit -> MUTEX
).

||FIELD = (n1:NEIGHBOR1 || n2:NEIGHBOR2 || {n1,n2}::FLAGS || MUTEX).

progress ENTER1 = {n1.enter} // NEIGHBOR 1 always gets to enter
progress ENTER2 = {n2.enter} // NEIGHBOR 2 always gets to enter

/*
Are there any adverse circumstances in which neighbors would not make progress?
What if the neighbors are greedy? Argue your answers.

Yes, if they raise their flags at the same time, they both will have to lower
their flags and try again, and this situation might repeat infinitely.
*/

/* Now we make them greedy (they prefer to raise their flag
to true instead of checking the other flag). */
||GREEDY = FIELD << {n1.flag1.setTrue, n2.flag2.setTrue}.

/*
GREEDY now has a progress violation:
Progress violation: ENTER2 ENTER1
Trace to terminal set of states:
	n2.flag2.setTrue
Cycle in terminal set:
	n1.flag1.setTrue
	n1.flag2.1
	n1.flag1.setFalse
Actions in terminal set:
	{n1.{flag1.{setFalse, setTrue}, flag2[1]}, n2.{flag1[1], flag2.{setFalse, setTrue}}}
*/
