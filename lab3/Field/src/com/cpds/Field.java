package com.cpds;

public class Field {
	public static void main(String[] args) {
		Flags flags = new Flags();

		/*
		Thread a = new Neighbor(flags);
		Thread b = new Neighbor(flags);
		*/

		Thread a = new NeighborGreedy(flags);
		Thread b = new NeighborGreedy(flags);

		a.setName("alice");
		b.setName("bob");

		a.start();
		b.start();
	}
}
